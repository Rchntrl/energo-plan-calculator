export function setDates(dateOld, dateNew, planId, kvt) {
    return {
        type: "PLAN_CHANGE",
        kvt: kvt,
        planId: planId,
        dateOld: dateOld,
        dateNew: dateNew
    }
}
