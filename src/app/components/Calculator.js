import React from "react";
import {PlanInformer} from "../components/PlanInformer";

export class Calculator extends React.Component {
    constructor(props) {
        super();
        this.state = {
            kvt: props.calc.kvt,
            planId: props.calc.planId,
            dateOld: props.calc.dateOld,
            dateNew: props.calc.dateNew
        };
    }

    onChangeDateOld(event) {
        this.setState({
            dateOld: event.target.value
        });
    }

    onChangeDateNew(event) {
        this.setState({
            dateNew: event.target.value
        });
    }

    onChangePlan(event) {
        this.setState({
            planId: event.target.value
        });
        this.props.changePlanDate(this.state.dateOld, this.state.dateNew, event.target.value, this.state.kvt);
    }

    onChangeKvt(event) {
        this.setState({
            kvt: event.target.value
        });
    }

    onClickButton() {
        this.props.changePlanDate(this.state.dateOld, this.state.dateNew, this.state.planId, this.state.kvt);
    }

    render() {
        return (
            <div className="col-xs-12">
                <div className="form">
                    <div className="form-group">
                        <label>Тариф: </label>
                        <select id="chose-plan" aria-placeholder="Выберите тариф" className="form-control"
                                value={this.props.planId} onChange={(event) => this.onChangePlan(event)}
                        >
                            {this.props.calc.plans.map((item, number) =>
                                <option key={number} value={number}>{item.title}</option>
                            )}
                        </select>
                    </div>
                    <PlanInformer plan={this.props.calc.plans[this.state.planId]}/>
                    <div className="form-group">
                        <label>Период: </label>
                        <div className="form-inline">
                            <input type="date" id="start-date" placeholder="Начало даты" className="form-control"
                                   value={this.props.dateOld} onChange={(event) => this.onChangeDateOld(event)}
                            />
                            <input type="date" id="end-date" placeholder="Конец даты" className="form-control"
                                   value={this.props.dateNew} onChange={(event) => this.onChangeDateNew(event)}
                            />
                        </div>
                    </div>
                    <div className="form-group">
                        <label>Расход: </label>
                        <div className="input-group">
                            <input type="number" id="consumption" placeholder="Расход кВт/ч" className="form-control"
                                   value={this.props.kvt}
                                   onChange={(event) => this.onChangeKvt(event)}
                            />
                            <span className="input-group-btn">
                                <button type="button" id="calculate-btn" className="btn btn-primary" onClick={() => this.onClickButton()}>OK</button>
                            </span>
                        </div>
                    </div>
                </div>
                <div className="result-board">
                    <dl className="col-xs-6">
                        <dt>Количество дней:</dt>
                        <dd>{this.props.calc.days}</dd>
                        <dt>Среднесуточная:</dt>
                        <dd>{this.props.calc.dayNormative}</dd>
                        <dt>Расход в пределах лимита:</dt>
                        <dd>{this.props.calc.lowerKvt}</dd>
                        <dt>Расход за пределами лимита:</dt>
                        <dd>{this.props.calc.upperKvt}</dd>
                    </dl>
                    <dl className="col-xs-6">
                        <dt>Сумма:</dt>
                        <dd>{this.props.calc.result}</dd>
                        <dt>НДС:</dt>
                        <dd>{this.props.calc.nds}</dd>
                        <dt>НсП:</dt>
                        <dd>{this.props.calc.nsp}</dd>
                    </dl>
                </div>
            </div>
        )
    }
}
