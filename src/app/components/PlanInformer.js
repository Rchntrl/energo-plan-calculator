import React from "react";

export const PlanInformer = (props) => {
    return (
        <table className="table">
            <tbody>
            <tr>
                <td>Регулярная цена</td>
                <td>{props.plan.regularPrice}</td>
            </tr>
            <tr>
                <td>Цена после превышения лимита</td>
                <td>{props.plan.upperPrice}</td>
            </tr>
            <tr>
                <td>Лимит потребления</td>
                <td>{props.plan.limit <= 9999999 ? props.plan.limit : 'Нет'}</td>
            </tr>
            </tbody>
        </table>
    )
};