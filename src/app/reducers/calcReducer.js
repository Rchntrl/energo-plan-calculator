var moment = require('moment');

const calcReducer = (state = {
    // input
    planId: 0,
    kvt: 0,
    // output
    dayNormative: 0,
    lowerKvt: 0,
    upperKvt: 0,
    result: 0,
    nds: 0,
    nsp: 0,
    days: 0,
    plans: [
        {
            title: "Население",
            limit: 700,
            regularPrice: 0.77,
            upperPrice: 2.16,
            nds: false,
            nsp: false,
        },
        {
            title: "Насосные станции для полива сельхоз. угодий",
            limit: 999999999,
            regularPrice: 0.779,
            upperPrice: 0.779,
            nds: false,
            nsp: true,
        },
        {
            title: "Потребители финансируемые из местного и республиканского бюджета",
            limit: 999999999,
            regularPrice: 2.24,
            upperPrice: 2.24,
            nds: true,
            nsp: true,
        },
        {
            title: "Промышленные потребители",
            limit: 999999999,
            regularPrice: 2.24,
            upperPrice: 2.24,
            nds: true,
            nsp: true,
        },
        {
            title: "Сельскохозяйственные потребители",
            limit: 999999999,
            regularPrice: 2.24,
            upperPrice: 2.24,
            nds: true,
            nsp: true,
        },
        {
            title: "Прочие потребители",
            limit: 999999999,
            regularPrice: 2.24,
            upperPrice: 2.24,
            nds: true,
            nsp: true,
        }
    ]
}, action) => {
    switch (action.type) {
        case "CALCULATE":
            let now = moment(action.dateNew), end = moment(action.dateOld);
            let days = now.diff(end, 'days');
            let dayNormative = action.kvt / days;
            let upperKvt = 0, lowerKvt = dayNormative * days;

            let plan = state.plans[action.planId];
            const socialNormative = plan.limit * 12 / 365;
            let sum = plan.regularPrice * lowerKvt;
            if (dayNormative > socialNormative) {
                lowerKvt = socialNormative * days;
                upperKvt = dayNormative * days - lowerKvt;
                sum  = plan.regularPrice * lowerKvt + plan.upperPrice * upperKvt;
            }

            state = {
                ...state,
                planId: action.planId,
                days: days,
                dayNormative:dayNormative.toFixed(3),
                lowerKvt: lowerKvt.toFixed(2),
                upperKvt: upperKvt.toFixed(2),
                nds: plan.nds ? (sum * 12/100).toFixed(2) : 0,
                nsp: plan.nsp ? (sum/100).toFixed(2) : 0,
                result: sum.toFixed(2),
            };
            break;
    }
    return state;
};

export default calcReducer;
