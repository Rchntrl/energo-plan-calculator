import React from "react";
import {connect} from "react-redux";

import {Calculator} from "../components/Calculator";

export class App extends React.Component {
    render() {
        return (
            <div className="container">
                <div className="col-xs-6">
                    <div className="row">
                    <Calculator calc={this.props.calc}
                            changePlanDate={(dateOld, dateNew, planId, kvt) => this.props.changePlanDate(dateOld, dateNew, planId, kvt)} />
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        calc: state.calc,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        changePlanDate: (dateOld, dateNew, planId, kvt) => {
            dispatch({
                type: "CALCULATE",
                kvt: kvt,
                planId: planId,
                dateOld: dateOld,
                dateNew: dateNew
            });
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
