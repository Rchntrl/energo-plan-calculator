
import {createStore, combineReducers, applyMiddleware} from "redux";
//import logger from "redux-logger";
import thunk from "redux-thunk";
import promise from "redux-promise-middleware";

import calc from "./reducers/calcReducer";

const MyLogger = (store) => (next) => (action) => {
    console.log("Logged Action: ", action);
    next(action);
};

export default createStore(
    combineReducers({
        calc,
    }),
    {},
    applyMiddleware(MyLogger, thunk, promise())
);
